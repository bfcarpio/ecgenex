# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :ec_genex,
  namespace: ECGenex,
  ecto_repos: [ECGenex.Repo],
  generators: [binary_id: true]

# Configures the endpoint
config :ec_genex, ECGenexWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "ZNXpKfN0N08wKdT/PIPv0GdgWIaFAVsHu3thw/yK6fOAsSAOkl4/3Ke1n4bIyY8Q",
  render_errors: [view: ECGenexWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: ECGenex.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
