use Mix.Config

# Configure your database
config :ec_genex, ECGenex.Repo,
  username: "postgres",
  password: "postgres",
  database: "ec_genex_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :ec_genex, ECGenexWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn
