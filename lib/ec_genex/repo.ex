defmodule ECGenex.Repo do
  use Ecto.Repo,
    otp_app: :ec_genex,
    adapter: Ecto.Adapters.Postgres
end
