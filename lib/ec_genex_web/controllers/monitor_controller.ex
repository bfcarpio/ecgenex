defmodule ECGenexWeb.MonitorController do
  use ECGenexWeb, :controller

  def show(conn, _) do
    text(conn, "API is up as of #{DateTime.utc_now()}")
  end
end
