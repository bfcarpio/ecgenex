defmodule ECGenexWeb.Router do
  use ECGenexWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
    plug :fetch_session
  end

  scope "/api", ECGenexWeb do
    pipe_through :api

    get "/", MonitorController, :show
  end
end
